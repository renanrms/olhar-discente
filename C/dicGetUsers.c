/* Universidade Federal do Rio de Janeiro
 * Escola Politecnica
 * Departamento de Eletronica e de Computacao
 * EEL270 - Computacao II - Turma 2016/1
 * Prof. Marcelo Luiz Drumond Lanza
 *
 * $Author: renan.passos $
 * $Date: 2019/06/03 08:04:06 $
 * $Log: dicGetUsers.c,v $
 * Revision 1.3  2019/06/03 08:04:06  renan.passos
 * funcionando
 *
 * Revision 1.2  2019/05/02 07:27:49  renan.passos
 * Correção de Bugs e implementação de Debugs no programa.
 *
 * Revision 1.1  2016/08/30 14:05:18  renan.passos
 * Initial revision
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dicGetUsers.h"
#include "dicFunctions.h"
#include "dicErrors.h"
#include "dicConfig.h"
#include "dicConst.h"
#include "dicTypes.h"

/*
 * dicErrorType
 * DicGetUsers (dicUserDataType**);
 *
 * Arguments:
 * dicUserDataType** - pointer to pointer to first user of list (O)
 *
 * Returned values:
 * dicOk - users list obtained successfully
 * dicInvalidArgument - received argument is NULL pointer
 * dicUsersFileNotExist - the users file not exist
 *
 * Description:
 * This function gives back a pointer to first user of a doubly linked list.
 * (If the users file is empty the function gives back a pointer to NULL)
 */
dicErrorType
DicGetUsers (dicUserDataType **dicFirstUser)
{
   FILE *dicUsersFile;
   char *validation;
   char *dicTokenPointer;
   char dicUserLine [DIC_USERSFILE_LINE_MAX_LENGTH + 1];
   dicUserDataType *dicUser;
   dicUserDataType *dicPreviousUser = NULL;

   #ifdef DEBUG
	 printf ("\E[1;33m\n\t  GetUsers: Entrou na função  *DEBUG text*\n\E[0m"); /*DEBUG*/
   #endif

   if (dicFirstUser == NULL)
      return dicInvalidArgument;

   dicUsersFile = fopen (DicGetAbsolutFileName (DIC_DATA_DIRECTORY, DIC_USERS_DATA_FILENAME), "r");
   if (dicUsersFile == NULL)
      return dicUsersFileNotExist;

   *dicFirstUser = NULL;

   #ifdef DEBUG
   printf ("\E[1;33m\n\t  GetUsers: Passou pelos tramentos de erro  *DEBUG text*\n\E[0m"); /*DEBUG*/
   #endif

   while (fgets (dicUserLine, DIC_USERSFILE_LINE_MAX_LENGTH + 1, dicUsersFile) != NULL)
   {

      #ifdef DEBUG
			printf ("\E[1;33m\n\t  GetUsers: Lendo uma linha do arquivo  *DEBUG text*\n\E[0m"); /*DEBUG*/
			#endif

			dicUser = malloc (sizeof (dicUserDataType));

			/*At the first line read, sets de FirstUser variable that is the output.*/
      if (dicPreviousUser == NULL)
			{
         *dicFirstUser = dicUser;
			}

      dicUser->userId = strtoul (strtok (dicUserLine, ":"), &validation, 10);

      #ifdef DEBUG
      printf ("\E[1;33m\n\t  GetUsers: Depois da leitura do userId=%lu  *DEBUG text*\n\E[0m", (unsigned long) dicUser->userId); /*DEBUG*/
      #endif

			strcpy (dicUser->nickname, strtok (NULL, ":"));

      #ifdef DEBUG
			printf ("\E[1;33m\n\t  GetUsers: Depois da leitura do nickname=%s  *DEBUG text*\n\E[0m", dicUser->nickname); /*DEBUG*/
      #endif

			dicTokenPointer = strtok (NULL, ":");
      if (*(dicTokenPointer - 1) == ':')
      {
         dicUser->password[0] = DIC_EOS;
         dicUser->profile = strtoul (dicTokenPointer, &validation, 10);
      }
      else
      {
				 strcpy (dicUser->password, dicTokenPointer);

         #ifdef DEBUG
				 printf ("\E[1;33m\n\t  GetUsers: Depois da leitura da senha=%s.\n", dicUser->password); /*DEBUG*/
         #endif

				 dicUser->profile = strtoul (strtok (NULL, ":"), &validation, 10);

         #ifdef DEBUG
				 printf ("\E[1;33m\n\t  GetUsers: Depois da leitura do perfil=%lu  *DEBUG text*\n\E[0m", (unsigned long) dicUser->profile); /*DEBUG*/
         #endif
			}

      strcpy (dicUser->username, strtok (NULL, ":"));

      #ifdef DEBUG
			printf ("\E[1;33m\n\t  GetUsers: Depois da leitura do nome de usuário=%s  *DEBUG text*\n\E[0m", dicUser->username); /*DEBUG*/
			#endif

      strcpy (dicUser->email, strtok (NULL, "\n"));

      #ifdef DEBUG
			printf ("\E[1;33m\n\t  GetUsers: Depois da leitura do email=%s  *DEBUG text*\n\E[0m", dicUser->email); /*DEBUG*/
      #endif

      dicUser->previous = dicPreviousUser;

      if (dicPreviousUser != NULL)
         dicPreviousUser->next = dicUser;

      dicPreviousUser = dicUser;
   }
   dicUser->next = NULL;

   fclose (dicUsersFile);

   return dicOk;
}


/*$RCSfile: dicGetUsers.c,v $*/
