/* Universidade Federal do Rio de Janeiro
 * Escola Politecnica
 * Departamento de Eletronica e de Computacao
 * EEL270 - Computacao II - Turma 2016/1
 * Prof. Marcelo Luiz Drumond Lanza
 *
 * $Author: renan.passos $
 * $Date: 2019/06/03 08:04:06 $
 * $Log: dicGetPendingRegistrationRequestsPerUser.c,v $
 * Revision 1.3  2019/06/03 08:04:06  renan.passos
 * funcionando
 *
 * Revision 1.2  2017/01/07 03:52:16  renan.passos
 * *** empty log message ***
 *
 * Revision 1.1  2016/08/30 14:05:18  renan.passos
 * Initial revision
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "dicGetPendingRegistrationRequestsPerUser.h"
#include "dicGetUsers.h"
#include "dicFunctions.h"
#include "dicErrors.h"
#include "dicTypes.h"
#include "dicConst.h"
#include "dicConfig.h"

/*
 * dicErrorType
 * DicGetPendingRegistrationRequestsPerUser (char*, dicUserDataType**);
 *
 * Arguments:
 * char* - responsible user nickname
 * dicUserDataType** - pointer to pointer to first element of
 * requesting users linked list (O)
 *
 * Returned values:
 * dicOk - get requestings successfully
 * dicInvalidArgument - one of the received arguments is NULL
 * pointer
 * dicUsersFileNotExist - the users file not exist
 * dicNicknameNotExist - the responsible user received not exist
 *
 * Description:
 * This function gives back a double linked list of register
 * requestings of responsible user with all your data.
 * (If there are't requestings in your responsability, gives
 * back NULL)
 */
dicErrorType
DicGetPendingRegistrationRequestsPerUser (char *dicResponsibleNickname, dicUserDataType **dicFirstRequestingUser)
{
	FILE *dicRequestingUsersFile;

	dicUserDataType *dicUser;
	dicUserDataType *dicPreviousUser;
	dicUserDataType *dicFirstUser;
	dicUserDataType *dicRequestingUser;

	dicUserIdentifierType dicResponsibleUserId;

	time_t dicAbsoluteValidityTime;
	dicUserIdentifierType dicUserId1;
	dicUserIdentifierType dicUserId2;
	char dicEncodedPassword [DIC_PASSWORD_MAX_LENGTH + 1];

	size_t dicReadBytes;

	dicErrorType dicReturnCode;

	#ifdef DEBUG
	printf("\E[1;33m\n\t  GetPendingRegistrationRequestsPerUser: Entrou na função  *DEBUG text*\n\E[0m"); /*DEBUG*/
	#endif

	if (dicFirstRequestingUser == NULL)
		return dicInvalidArgument;

	*dicFirstRequestingUser = NULL;
	dicRequestingUser = NULL;

	#ifdef DEBUG
	printf("\E[1;33m\n\t  GetPendingRegistrationRequestsPerUser: Após o tratamento de erro  *DEBUG text*\n\E[0m"); /*DEBUG*/
	#endif

	dicReturnCode = DicGetUsers (&dicFirstUser);
	if (dicReturnCode != dicOk)
		return dicReturnCode;

	#ifdef DEBUG
	printf("\E[1;33m\n\t  GetPendingRegistrationRequestsPerUser: Depois da chamada a GetUsers  *DEBUG text*\n\E[0m"); /*DEBUG*/
	#endif

	dicUser = dicFirstUser;
	while (dicUser != NULL)
	{
		if (!strcmp(dicUser->nickname, dicResponsibleNickname))
		{
			dicResponsibleUserId = dicUser->userId;
			break;
		}
		dicUser = dicUser->next;
	}

	dicRequestingUsersFile = fopen (DicGetAbsolutFileName (DIC_DATA_DIRECTORY, DIC_REQUESTING_USERS_DATA_FILENAME), "r");
	if (dicRequestingUsersFile == NULL)
		return dicOk;

	/*<validity><responsible userId><requesting userId><encoded password>*/
	dicReadBytes = 0;
	dicReadBytes += sizeof (time_t)                * fread (&(dicAbsoluteValidityTime), sizeof (time_t), 1, dicRequestingUsersFile);
	dicReadBytes += sizeof (dicUserIdentifierType) * fread (&(dicUserId1), sizeof (dicUserIdentifierType), 1, dicRequestingUsersFile);
	dicReadBytes += sizeof (dicUserIdentifierType) * fread (&(dicUserId2), sizeof (dicUserIdentifierType), 1, dicRequestingUsersFile);
	dicReadBytes += sizeof (char)                  * fread (dicEncodedPassword, sizeof (char), DIC_PASSWORD_MAX_LENGTH, dicRequestingUsersFile);
	dicEncodedPassword [DIC_PASSWORD_MAX_LENGTH] = DIC_EOS;

	/*obtains the list of not expired requests*/
	while (dicReadBytes == sizeof (time_t) + 2*sizeof (dicUserIdentifierType) + DIC_PASSWORD_MAX_LENGTH*sizeof (char))
	{
		#ifdef DEBUG
		printf("\E[1;33m\n\t  GetPendingRegistrationRequestsPerUser: Lendo uma linha do arquivo 'requesting.users'  *DEBUG text*\n\E[0m"); /*DEBUG*/
		#endif

		#ifdef DEBUG
		printf("\E[1;33m\n\t  GetPendingRegistrationRequestsPerUser: validity=%lld, responsibleUid=%llu, requestingUid=%llu, encoded Password=%s  *DEBUG text*\n\E[0m", (long long) dicAbsoluteValidityTime, dicUserId1, dicUserId2, dicEncodedPassword); /*DEBUG*/
		#endif

		/*if request is not expired and the responsible by the request is the wanted*/
		if (time (NULL) <= dicAbsoluteValidityTime && dicUserId1 == dicResponsibleUserId)
		{
			if (dicRequestingUser != NULL)
			{
				dicRequestingUser->next = malloc (sizeof (dicUserDataType));
				dicRequestingUser = dicRequestingUser->next;
			}
			else
			{
				dicRequestingUser = malloc (sizeof (dicUserDataType));
				dicRequestingUser->previous = NULL;
			}

			dicRequestingUser->userId = dicUserId2;
			strcpy (dicRequestingUser->password, dicEncodedPassword);

			/*if is the first not expired request*/
			if (*dicFirstRequestingUser == NULL)
				*dicFirstRequestingUser = dicRequestingUser;
		}
		else
		{
			#ifdef DEBUG
			printf("\E[1;33m\n\t  GetPendingRegistrationRequestsPerUser: Solicitação de registro expirada  *DEBUG text*\n\E[0m"); /*DEBUG*/
			#endif
		}

		/*<validity><responsible userId><requesting userId><encoded password>*/
		dicReadBytes = 0;
		dicReadBytes += sizeof (time_t)                * fread (&(dicAbsoluteValidityTime), sizeof (time_t), 1, dicRequestingUsersFile);
		dicReadBytes += sizeof (dicUserIdentifierType) * fread (&(dicUserId1), sizeof (dicUserIdentifierType), 1, dicRequestingUsersFile);
		dicReadBytes += sizeof (dicUserIdentifierType) * fread (&(dicUserId2), sizeof (dicUserIdentifierType), 1, dicRequestingUsersFile);
		dicReadBytes += sizeof (char)                  * fread (dicEncodedPassword, sizeof (char), DIC_PASSWORD_MAX_LENGTH, dicRequestingUsersFile);
		dicEncodedPassword [DIC_PASSWORD_MAX_LENGTH] = DIC_EOS;
	
	}
	fclose (dicRequestingUsersFile);
	if (dicRequestingUser != NULL)
		dicRequestingUser->next = NULL;

	dicUser = dicFirstUser;
	/*obtain data of requesting users*/
	while (dicUser != NULL)
	{
		
		#ifdef DEBUG
		printf("\E[1;33m\n\t  GetPendingRegistrationRequestsPerUser: Olhando para userId=%llu  *DEBUG text*\n\E[0m", dicUser->userId); /*DEBUG*/
		#endif
		
		dicRequestingUser = *dicFirstRequestingUser;
		while (dicRequestingUser != NULL)
		{

			#ifdef DEBUG
			printf("\E[1;33m\n\t\t  GetPendingRegistrationRequestsPerUser: requesting userId=%llu  *DEBUG text*\n\E[0m", dicRequestingUser->userId); /*DEBUG*/
			#endif
		
			if (dicRequestingUser->userId == dicUser->userId)
			{
				strcpy (dicRequestingUser->nickname, dicUser->nickname);
				dicRequestingUser->profile = dicUser->profile;
				strcpy (dicRequestingUser->username, dicUser->username);
				strcpy (dicRequestingUser->email, dicUser->email);

				#ifdef DEBUG
				printf("\E[1;33m\n\t  GetPendingRegistrationRequestsPerUser: Dados gravados em requesting user.  *DEBUG text*\n\E[0m"); /*DEBUG*/
				#endif
			}

			dicRequestingUser = dicRequestingUser->next;
		}

		dicUser = dicUser->next;
	}

	#ifdef DEBUG
	printf("\E[1;33m\n\t  GetPendingRegistrationRequestsPerUser: Depois de copiar os dados dos solicitantes de registro.  *DEBUG text*\n\E[0m"); /*DEBUG*/
	#endif

	dicUser = dicFirstUser;
	while (dicUser != NULL)
	{
		dicPreviousUser = dicUser;
		dicUser = dicUser->next;
		free (dicPreviousUser);
	}

	#ifdef DEBUG
	printf("\E[1;33m\n\t  GetPendingRegistrationRequestsPerUser: Depois de desalocar a memória.  *DEBUG text*\n\E[0m"); /*DEBUG*/
	#endif

	return dicOk;
}

/*$RCSfile: dicGetPendingRegistrationRequestsPerUser.c,v $*/
